# simhashr: Similarity min-hashing
Copyright (c) 2018 Bart Massey  
`bart.massey@gmail.com`

This Rust library crate computes a similarity min-hash for a
file using a rolling CRC-32.


