// Copyright © 2018 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

//! Compute CRC min-hash for a given object. These are
//! useful in similarity comparisons using the Jacquard
//! metric. This code is at heart a modernized
//! Rust port of <http://github.com/BartMassey/simhash>.
//!
//! Bibliography
//!
//!   Mark Manasse  
//!   Microsoft Research Silicon Valley  
//!   Finding similar things quickly in large collections  
//!   http://research.microsoft.com/research/sv/PageTurner/similarity.htm
//!
//!   Andrei Z. Broder  
//!   On the resemblance and containment of documents  
//!   In Compression and Complexity of Sequences (SEQUENCES'97),  
//!   pages 21-29. IEEE Computer Society, 1998  
//!   ftp://ftp.digital.com/pub/DEC/SRC/publications/broder/  
//!     positano-final-wpnums.pdf
//!
//!   Andrei Z. Broder  
//!   Some applications of Rabin's fingerprinting method  
//!   Published in R. Capocelli, A. De Santis, U. Vaccaro eds.  
//!   Sequences II: Methods in Communications, Security, and  
//!   Computer Science, Springer-Verlag, 1993.  
//!   http://athos.rutgers.edu/~muthu/broder.ps

extern crate rolling_crc;
use rolling_crc::*;

use std::collections::BinaryHeap;
use std::fs;
use std::io::{self, Read};
use std::iter::Iterator;
use std::path;

/// Min-hash of a byte object.
pub struct MinHash {
    pub window_size: usize,
    pub hashes: Vec<u32>,
}

impl MinHash {

    /// Create a new `MinHash` from the given `bytes` using
    /// the given sizes. The sizes must be greater than 0.
    pub fn from_iterator(
        window_size: usize,
        hash_size: usize,
        bytes: &mut Iterator<Item=u8>,
    ) -> MinHash
    {
        assert!(hash_size > 0);
        assert!(window_size > 0);
        let context = RollingCRCContext::new(window_size);
        let rolling_crc = RollingCRC::new(&context);
        let mut hashes: BinaryHeap<u32> =
            BinaryHeap::new();
        for (_, crc) in rolling_crc.iter(bytes) {
            if hashes.len() < hash_size {
                hashes.push(crc);
                continue;
            }
            if crc < *hashes.peek().unwrap() {
                let _ = hashes.pop();
                hashes.push(crc);
                continue;
            }
        }
        let hashes = hashes.into_sorted_vec();
        MinHash{window_size, hashes}
    }

    /// Compute a `MinHash` of the full contents of the
    /// given reader.
    pub fn from_read<T: Read>(
        window_size: usize,
        hash_size: usize,
        mut read: T,
    ) -> Result<MinHash, io::Error>
    {
        let mut fbytes = Vec::new();
        read.read_to_end(&mut fbytes)?;
        Ok(MinHash::from_iterator(
            window_size,
            hash_size,
            &mut fbytes.into_iter(),
        ))
    }

    /// Compute a `MinHash` of the full contents of the
    /// file at the given filename.
    pub fn from_file<T: AsRef<path::Path>>(
        window_size: usize,
        hash_size: usize,
        filename: T,
    ) -> Result<MinHash, io::Error>
    {
        let r = fs::File::open(filename)?;
        MinHash::from_read(window_size, hash_size, r)
    }
}
